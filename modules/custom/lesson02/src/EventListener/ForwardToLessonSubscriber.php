<?php

namespace Drupal\lesson02\EventListener;

use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class ForwardToLessonSubscriber
 * @package Drupal\lesson02\EventListener
 */
class ForwardToLessonSubscriber implements EventSubscriberInterface
{
    /**
     * @var ConfigFactoryInterface
     */
    private $configFactory;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    /**
     * ForwardToLessonSubscriber constructor.
     * @param ConfigFactoryInterface $configFactory
     * @param UrlGeneratorInterface $urlGenerator
     */
    public function __construct(ConfigFactoryInterface $configFactory, UrlGeneratorInterface $urlGenerator)
    {
        $this->configFactory = $configFactory;
        $this->urlGenerator  = $urlGenerator;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['onKernelView', PHP_INT_MAX]
        ];
    }

    /**
     * @param GetResponseForControllerResultEvent $event
     */
    public function onKernelView(GetResponseForControllerResultEvent $event)
    {
        $config = $this->configFactory->get('lesson02.settings');
        if (in_array($event->getRequest()->get('_route'), $config->getOriginal('referrer_route_list'))) {

            $result = $event->getControllerResult();
            $result['#markup'] = sprintf(
                '%s<br/><a href="%s">Please, move to Lesson #2</a>',
                $result['#markup'],
                $this->urlGenerator->generate($config->get('forward_to_route'))
            );

            $event->setControllerResult($result);
        }
    }
}
