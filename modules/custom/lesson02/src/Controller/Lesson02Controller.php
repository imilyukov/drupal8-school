<?php

namespace Drupal\lesson02\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class Lesson01Controller
 * @package Drupal\lesson02\Controller
 */
class Lesson02Controller extends ControllerBase
{
    /**
     * @return array
     */
    public function index()
    {
        return [
            '#title'  => "Lesson #2",
            '#markup' => "This is front for the second lesson.",
        ];
    }

    /**
     * @param Request $request
     * @return array
     */
    public function checkStudent(Request $request)
    {
        $referrer = $request->headers->get('referer');
        $config   = $this->config('lesson02.settings');

        $markup = '';
        if ($referrer) {
            $markup = sprintf('You went from "%s"', $referrer);
        }

        $referrerList = array_map(function ($route) {
            return $this->getUrlGenerator()->generate($route, [], UrlGeneratorInterface::ABSOLUTE_URL);
        }, $config->get('referrer_route_list'));

        if (in_array($referrer, $referrerList)) {

            $markup = sprintf(
                '<div class="alert alert-success">
                    <strong>Success!</strong> You are finished with previous lesson.
                </div><br/>%s',
                $markup
            );
        }

        return [
            '#title'  => "Checking student from forward page.",
            '#markup' => $markup,
        ];
    }
}
