<?php

namespace Drupal\lesson01\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class Lesson01Controller
 * @package Drupal\lesson01\Controller
 */
class Lesson01Controller extends ControllerBase
{
    /**
     * @return array
     */
    public function index()
    {
        return [
            '#title'  => "Lesson #1",
            '#markup' => "This is front for the first lesson.",
        ];
    }

    /**
     * @return array
     */
    public function welcomeMessage()
    {
        return [
            '#title'  => "Welcome to Drupal 8 School",
            '#markup' => "This is introduction lesson about Drupal 8 architecture.",
        ];
    }
}
