<?php

/**
 * @file
 * Contains \DrupalProject\Composer\DirectoryHandler.
 */

namespace Drupal\Scripts\Composer;

use Composer\Script\Event;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class InstallHandler
 * @package Drupal\Scripts\Composer
 */
class InstallHandler
{
    /**
     * @param Event $event
     */
    public static function createRequiredFiles(Event $event)
    {
        $fs   = new Filesystem();
        $root = __DIR__ . '/../..';

        // Required for unit testing
        foreach (['modules', 'profiles', 'themes'] as $dir) {

            foreach (['contrib', 'custom'] as $scope) {

                if (!$fs->exists($root . '/' . $dir . '/' . $scope)) {
                    $fs->mkdir($root . '/' . $dir . '/' . $scope);
                    $fs->touch($root . '/' . $dir . '/' . $scope . '/.gitkeep');
                }
            }
        }

        // Prepare the settings file for installation
        if (!$fs->exists($root . '/sites/default/settings.php')) {
            $fs->copy($root . '/sites/default/default.settings.php', $root . '/sites/default/settings.php');
            $fs->chmod($root . '/sites/default/settings.php', 0666);
            $event->getIO()->write("Create a sites/default/settings.php file with chmod 0666");
        }

        // Prepare the services file for installation
        if (!$fs->exists($root . '/sites/default/services.yml')) {
            $fs->copy($root . '/sites/default/default.services.yml', $root . '/sites/default/services.yml');
            $fs->chmod($root . '/sites/default/services.yml', 0666);
            $event->getIO()->write("Create a sites/default/services.yml file with chmod 0666");
        }

        // Create the files directory with chmod 0777
        if (!$fs->exists($root . '/sites/default/files')) {
            $oldmask = umask(0);
            $fs->mkdir($root . '/sites/default/files', 0777);
            umask($oldmask);
            $event->getIO()->write("Create a sites/default/files directory with chmod 0777");
        }

        $files = glob($root . '/sites/*/files/*', GLOB_ONLYDIR);
        foreach ($files as $directory) {

            if (!preg_match('/images|css|js$/i', $directory)) {
                continue;
            }

            $fs->symlink(
                $directory,
                sprintf(
                    '%s/web/%s',
                    $root,
                    str_replace($root, '', $directory)
                )
            );
        }

        $files = glob($root . '/core/themes/*/*');
        foreach ($files as $directory) {

            $fs->symlink(
                $directory,
                sprintf(
                    '%s/web/%s',
                    $root,
                    str_replace($root, '', $directory)
                )
            );
        }

        $fs->symlink(
            $root . '/core/assets',
            $root . '/web/core/assets'
        );

        $fs->symlink(
            $root . '/core/misc',
            $root . '/web/core/misc'
        );
    }
}
