<?php

/**
 * @file
 * The PHP page that serves all page requests on a Drupal installation.
 *
 * All Drupal code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt files in the "core" directory.
 */

use Drupal\Core\DrupalKernel;
use Symfony\Component\HttpFoundation\Request;

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

$loader = require_once __DIR__ . '/../autoload.php';

$request = Request::createFromGlobals();
$kernel  = new DrupalKernel(getenv('SITE_ENV') ?: 'dev', $loader);

$response = $kernel->handle($request);
$response->send();

$kernel->terminate($request, $response);
